using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class LevelCompleted : MonoBehaviour
{
	public GameObject scoreUI;
	public GameObject levelCompletedUI;
	public GameObject levelLoader;
	public TextMeshProUGUI levelName;
	public Button nextLevelBtn;

	private static bool levelCompleted;
	private static bool levelWon;

	private void Start()
	{
		levelCompletedUI.SetActive(false);
		levelName.text = SceneManager.GetActiveScene().name;
		Time.timeScale = 1f;
		nextLevelBtn.interactable = true;
	}

	void Update()
    {
		levelCompleted = scoreUI.GetComponent<ScoreScript>().getLevelCompleted();
		levelWon = scoreUI.GetComponent<ScoreScript>().getLevelWon();

		if (levelCompleted)
		{
			//Play levelcompleted audio clip
			//FindObjectOfType<AudioManager>().Play("LevelCompleted");
			ShowMenu();
		}
    }

	private void ShowMenu()
	{
		levelCompletedUI.SetActive(true);
		Time.timeScale = 0f;
		
		if (!levelWon || SceneManager.GetActiveScene().name == "Level5")
		{
			nextLevelBtn.interactable = false;
		}
	}

	public void LoadNextLevel()
	{
		int levelIndex = SceneManager.GetActiveScene().buildIndex;
		SceneManager.LoadScene(levelIndex + 1);
		//levelLoader.GetComponent<LevelLoader>().LoadNextLevel();
	}

	public void RestartLevel()
	{
		int levelIndex = SceneManager.GetActiveScene().buildIndex;
		SceneManager.LoadScene(levelIndex);
	}

	public void LoadMenu()
	{
		SceneManager.LoadScene(sceneName: "MainMenu");
	}
}
