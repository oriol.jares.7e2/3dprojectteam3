using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
	public static LevelManager instance;

	private bool canPlayLvl2;
	private bool canPlayLvl3;
	private bool canPlayLvl4;
	private bool canPlayLvl5;

	void Awake()
	{
		if (instance == null)
			instance = this;
		else
		{
			Destroy(gameObject);
			return;
		}
		DontDestroyOnLoad(gameObject);
	}

	public bool getCanPlayLvl2()
	{
		return canPlayLvl2;
	}

	public bool getCanPlayLvl3()
	{
		return canPlayLvl3;
	}

	public bool getCanPlayLvl4()
	{
		return canPlayLvl4;
	}

	public bool getCanPlayLvl5()
	{
		return canPlayLvl5;
	}

	public void setCanPlayLvl2(bool canplay)
	{
		canPlayLvl2 = canplay;
	}

	public void setCanPlayLvl3(bool canplay)
	{
		canPlayLvl3 = canplay;
	}

	public void setCanPlayLvl4(bool canplay)
	{
		canPlayLvl4 = canplay;
	}

	public void setCanPlayLvl5(bool canplay)
	{
		canPlayLvl5 = canplay;
	}


}
