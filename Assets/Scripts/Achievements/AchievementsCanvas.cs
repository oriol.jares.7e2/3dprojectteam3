﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementsCanvas : MonoBehaviour
{

    //Variables generales

    public bool logroActivo = false;

    public GameObject logro_panel;
    public GameObject logro_logo;
    public GameObject logro_titulo;
    public GameObject logro_desc;

    public IEnumerator Trigger02()
    {

        if (FindObjectOfType<AchievementsManager>().getCompletado02() == false){
            logro_panel.SetActive(true);
            logroActivo = true;
            logro_titulo.GetComponent<Text>().text = "Primeros pasos";
            logro_desc.GetComponent<Text>().text = "Consigue 3 estrellas por primera vez";
            FindObjectOfType<AchievementsManager>().setCompletado02(true);
            yield return new WaitForSeconds(7);
            //Resetear UI
            logro_panel.SetActive(false);
            logroActivo = false;
        }

    }

}
