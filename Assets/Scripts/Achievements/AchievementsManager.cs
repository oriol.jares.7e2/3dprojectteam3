﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AchievementsManager : MonoBehaviour
{

    //Variables generales

    private bool completado01 = false;
    private bool completado02 = false;

    public static AchievementsManager instance;

    // Update is called once per frame

    void Awake()
    {
        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
    }

    // Getters y Setters

    public bool getCompletado01()
    {
        return completado01;
    }

    public bool getCompletado02()
    {
        return completado02;
    }

    public void setCompletado01(bool completado01)
    {
        this.completado01 = completado01;
    }

    public void setCompletado02(bool completado02)
    {
        this.completado02 = completado02;
    }

}
