using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class ScoreScript : MonoBehaviour
{
    public LayerMask casillaLayerMask;
    public GameObject player;
    public RaycastHit hit;
    public int threeStars = 12;
    public int twoStars = 15;
    private string sampleText;
	private bool levelCompleted = false;
	private bool levelWon = false;

	public Image stars;
	public Sprite star1;
	public Sprite star2;
	public Sprite star3;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }
    // Update is called once per frame
    void Update()
    {
        bool raycastHit = Physics.Raycast(player.transform.position, Vector3.down, out hit, 5f, casillaLayerMask);

        if (raycastHit)
        {
            GameObject casilla = hit.collider.gameObject;
            int movements = player.GetComponent<PlayerMovement>().getterMovCounter();
            if (casilla.CompareTag("Scorerelevancy")) {
				levelCompleted = true;
				if (movements < threeStars) {
					stars.sprite = star3;
					levelWon = true;
					checkCurrentLvl();
					StartCoroutine(GameObject.Find("LevelCanvas").GetComponent<AchievementsCanvas>().Trigger02());
				}
                else if (movements < twoStars)
                {
					stars.sprite = star2;
					levelWon = true;
					checkCurrentLvl();
				}
                else {
					stars.sprite = star1;
					levelWon = false;
				}
            } else if (casilla.CompareTag("Tutorial"))
			{
				int levelIndex = SceneManager.GetActiveScene().buildIndex;
				SceneManager.LoadScene(levelIndex + 1);
			}
        }
    }

	public bool getLevelCompleted()
	{
		return levelCompleted;
	}

	public bool getLevelWon()
	{
		return levelWon;
	}

	private void checkCurrentLvl()
	{
		if (SceneManager.GetActiveScene().name == "Level1")
		{
			FindObjectOfType<LevelManager>().setCanPlayLvl2(true);
		}
		else if (SceneManager.GetActiveScene().name == "Level2")
		{
			FindObjectOfType<LevelManager>().setCanPlayLvl3(true);
		}
		else if (SceneManager.GetActiveScene().name == "Level3")
		{
			FindObjectOfType<LevelManager>().setCanPlayLvl4(true);
		}
		else if (SceneManager.GetActiveScene().name == "Level4")
		{
			FindObjectOfType<LevelManager>().setCanPlayLvl5(true);
		}
	}
}
