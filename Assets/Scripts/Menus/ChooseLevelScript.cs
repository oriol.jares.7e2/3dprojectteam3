using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ChooseLevelScript : MonoBehaviour
{
	private bool canPlayLvl2 = false;
	private bool canPlayLvl3 = false;
	private bool canPlayLvl4 = false;
	private bool canPlayLvl5 = false;

	public Button lvl2Btn;
	public Button lvl3Btn;
	public Button lvl4Btn;
	public Button lvl5Btn;

	private void Update()
	{
		canPlayLvl2 = FindObjectOfType<LevelManager>().getCanPlayLvl2();
		canPlayLvl3 = FindObjectOfType<LevelManager>().getCanPlayLvl3();
		canPlayLvl4 = FindObjectOfType<LevelManager>().getCanPlayLvl4();
		canPlayLvl5 = FindObjectOfType<LevelManager>().getCanPlayLvl5();

		CanPlayLvl(lvl2Btn, canPlayLvl2);
		CanPlayLvl(lvl3Btn, canPlayLvl3);
		CanPlayLvl(lvl4Btn, canPlayLvl4);
		CanPlayLvl(lvl5Btn, canPlayLvl5);
	}

	public void back()
	{
		SceneManager.LoadScene(sceneName: "StartGame");
	}

	public void GoToLevel(int lvl)
	{
		string level = "Level" + lvl.ToString();
		SceneManager.LoadScene(sceneName: level);
	}

	private void CanPlayLvl(Button btn, bool canplay)
	{
		if (canplay)
		{
			btn.interactable = true;
		}
	}
}
