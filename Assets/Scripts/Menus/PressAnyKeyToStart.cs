﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PressAnyKeyToStart : MonoBehaviour
{
    
    void Update()
    {
        if (Input.anyKey)
        {
            SceneManager.LoadScene(sceneName: "MainMenu");
        }
    }
}
