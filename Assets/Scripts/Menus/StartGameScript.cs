using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGameScript : MonoBehaviour
{
	public void back()
	{
		SceneManager.LoadScene(sceneName: "MainMenu");
	}

	public void NewGame()
	{
		SceneManager.LoadScene(sceneName: "Level1");
	}

	public void ChooseLevel()
	{
		SceneManager.LoadScene(sceneName: "ChooseLevel");
	}

	public void LoadTutorial()
	{
		SceneManager.LoadScene(sceneName: "Tutorial1");
	}
}
