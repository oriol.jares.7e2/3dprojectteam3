using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuNav : MonoBehaviour
{
    public void menuToChooseLvl()
    {
		//FindObjectOfType<LevelLoader>().LoadNextLevel();
		SceneManager.LoadScene(sceneName: "StartGame");
	}

    public void menuToSettings()
    {
        SceneManager.LoadScene(sceneName: "Settings");
    }

    public void quit()
    {
        Application.Quit();
    }
}
