﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotator : MonoBehaviour
    {
    //Variables
    public float speed;
    public GameObject target;
    //Lock de direccion
    private Vector3 direction = new Vector3(0f, 1f,0f);


    

    private void Update()
    {
       
        //Click derecho
        if (Input.GetMouseButton(2)) {
            //EL avlor de Input ha de ser negativo para que vaya a en al dirección contraria
            transform.RotateAround(target.transform.position, direction, Input.GetAxis("Mouse X") * speed );
        }
    }
}
