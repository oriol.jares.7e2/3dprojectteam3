﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotesCount : MonoBehaviour
{

    public int count;
    public static NotesCount Instance;

    void Start()
    {
        count = 0;
    }

    void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    public int getNotesCount()
    {
        return count;
    }

    public void resetNotesCount()
    {
        count = 0;
    }

    public void notesUp()
    {
        count++;
    }
}
