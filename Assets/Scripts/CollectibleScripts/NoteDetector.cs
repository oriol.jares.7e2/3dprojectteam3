using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class NoteDetector : MonoBehaviour
{
    private NotesCount count;

    void Awake()
    {
        count = GameObject.Find("NotesCount").GetComponent<NotesCount>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            //Play pick up audio clip
            FindObjectOfType<AudioManager>().Play("Pickup");

            count.notesUp();
            Destroy(this.gameObject);
        }
    }
}
