using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    private NotesCount count;

    void Awake()
    {
        count = GameObject.Find("NotesCount").GetComponent<NotesCount>();
    }

    public void reloadCurrentScene()
    {
		//Play restart audio clip
		FindObjectOfType<AudioManager>().Play("Death");
		count.resetNotesCount();
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }
}
