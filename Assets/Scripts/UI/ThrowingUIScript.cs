using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowingUIScript : MonoBehaviour
{
	public GameObject throwingUI;
	public GameObject player;
	private bool isThrowing;

	void Update()
    {
		isThrowing = player.GetComponent<PlayerThrow>().getIfThrowing();

		if (isThrowing)
		{
			throwingUI.SetActive(true);
		} else
		{
			throwingUI.SetActive(false);
		}
    }
}
