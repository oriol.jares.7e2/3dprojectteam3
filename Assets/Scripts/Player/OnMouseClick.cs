using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class OnMouseClick : MonoBehaviour
{
    public LayerMask caminoLayerMask;
    public LayerMask enemyLayerMask;
    public GameObject rCR;
    public GameObject rCY;
    public GameObject rCG;
    public GameObject rCB;
    public float distance = 2.5f;

    private static bool hiding;
    private bool throwing;
    private bool moving = false;
    public bool inRange;

    private float timesMoved;
    private float distanceMoved = 0f;
    private GameObject playerChar;
    private Vector3 playerVector3;
    private Vector3 vector3Final;
    private Vector3 direction;
    private Vector3 casillaPos;

	public GameObject paperBallPS;

	public Animator locker;

    private void Update()
    {
        throwing = GameObject.Find("Player").GetComponent<PlayerThrow>().getIfThrowing();
    }

    private void OnMouseDown()
    {
        if (!throwing)
        {
            //Move

            if (this.gameObject.name == GameObject.Find("Player").GetComponent<PlayerMovement>().checkField(rCR))
            {
                bool raycastHit = Physics.Raycast(rCR.transform.position, Vector3.left, 2f, caminoLayerMask);
                checkRaycastHit(raycastHit);
            }
            else if (this.gameObject.name == GameObject.Find("Player").GetComponent<PlayerMovement>().checkField(rCY))
            {
                bool raycastHit = Physics.Raycast(rCY.transform.position, Vector3.back, 2f, caminoLayerMask);
                checkRaycastHit(raycastHit);
            }
            else if (this.gameObject.name == GameObject.Find("Player").GetComponent<PlayerMovement>().checkField(rCG))
            {
                bool raycastHit = Physics.Raycast(rCG.transform.position, Vector3.right, 2f, caminoLayerMask);
                checkRaycastHit(raycastHit);
            }
            else if (this.gameObject.name == GameObject.Find("Player").GetComponent<PlayerMovement>().checkField(rCB))
            {
                bool raycastHit = Physics.Raycast(rCB.transform.position, Vector3.forward, 2f, caminoLayerMask);
                checkRaycastHit(raycastHit);
            }
        } else
        {
            if (inRange)
            {
                //Play throw audio clip
                FindObjectOfType<AudioManager>().Play("Throw");
				Instantiate(paperBallPS, this.gameObject.transform.position, Quaternion.identity);

				Collider[] enemies = Physics.OverlapSphere(this.gameObject.transform.position, 3f, enemyLayerMask);
                for (int i = 0; i < enemies.Length; i++)
                {
                    
                    if (enemies[i].gameObject.name == "BullyCylinder")
                    {
                        //Hace falta implementar pasar por parametro que enemigos estan a rango de la casilla
                        enemyToCasilla(this.gameObject);
                    }
                }
            }
        }
    }

    Vector3 oldVector3;

    private void move()
    {
		//Play move audio clip
		FindObjectOfType<AudioManager>().Play("Move");
		casillaPos = new Vector3(this.gameObject.transform.position.x, GameObject.Find("Player").transform.position.y, this.gameObject.transform.position.z);
        playerChar = GameObject.Find("Player");
        if (!moving)
        {
            timesMoved = (2.5f * 0.02f) / 0.5f;
            distanceMoved = 0f;
            moving = true;
            GameObject.Find("Player").GetComponent<PlayerMovement>().setIfMoving(true);
            playerVector3 = playerChar.transform.position;
            oldVector3 = playerVector3;
            vector3Final = casillaPos - playerVector3;
            direction = vector3Final.normalized;
            playerChar.GetComponent<PlayerMovement>().movCounterUp();
        }
    }

    private void checkIfHiding()
    {
        if (this.gameObject.CompareTag("Hiding"))
        {
            GameObject.Find("Body").GetComponent<MeshRenderer>().enabled = false;
            GameObject.Find("Body").GetComponent<SphereCollider>().enabled = false;
			GameObject.Find("Head").GetComponent<MeshRenderer>().enabled = false;
			GameObject.Find("Head").GetComponent<SphereCollider>().enabled = false;
			GameObject.Find("Player").GetComponent<PlayerMovement>().setIsHiding(true);
            hiding = true;
			//Play locker audio clip
			FindObjectOfType<AudioManager>().Play("Locker");
			locker.SetTrigger("Open");
		} else
        {
			GameObject.Find("Body").GetComponent<MeshRenderer>().enabled = true;
			GameObject.Find("Body").GetComponent<SphereCollider>().enabled = true;
			GameObject.Find("Head").GetComponent<MeshRenderer>().enabled = true;
			GameObject.Find("Head").GetComponent<SphereCollider>().enabled = true;
			GameObject.Find("Player").GetComponent<PlayerMovement>().setIsHiding(false);
            hiding = false;
        }
    }

    private void checkRaycastHit(bool hit)
    {
        if (hit)
        {
            move();
            enemyMovement();
        }
    }

    public bool getIfHiding()
    {
        return hiding;
    }

    public bool getIfMoving()
    {
        return moving;
    }

    public void setIsInRange(bool range)
    {
        inRange = range;
    }

    public void enemyMovement()
    {
        GameObject.FindWithTag("Bully").GetComponent<EnemyMovement>().moveEnemy();
    }

    public void enemyToCasilla(GameObject gameObject)
    {
        GameObject.FindWithTag("Bully").GetComponent<EnemyMovement>().moveToCasilla(gameObject);
    }

    void FixedUpdate()
    {
        if (moving && playerVector3.x != casillaPos.x || playerVector3.y != casillaPos.y || playerVector3.z != casillaPos.z)
        {
            if (hiding && playerVector3 != casillaPos)
            {
                checkIfHiding();
            }
            float newXValue = (float)System.Math.Round(playerVector3.x + (timesMoved * direction.x), 2);
            float newYValue = (float)System.Math.Round(playerVector3.y + (timesMoved * direction.y), 2);
            float newZValue = (float)System.Math.Round(playerVector3.z + (timesMoved * direction.z), 2);
            playerVector3 = new Vector3(newXValue, newYValue, newZValue);
            playerChar.transform.position = playerVector3;
            if (playerVector3 == casillaPos)
            {
                moving = false;
                GameObject.Find("Player").GetComponent<PlayerMovement>().setIfMoving(false);
                checkIfHiding();
            }
        }
    }

}
