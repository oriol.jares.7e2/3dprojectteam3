﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public LayerMask casillaLayerMask;
    public GameObject rCR;
    public GameObject rCY;
    public GameObject rCG;
    public GameObject rCB;
    private bool moving;
    public int movCounter;
    public bool hiding;

    public string checkField(GameObject pos)
    {
        RaycastHit hit;
        string nameCasilla = "";
        bool raycastHit = Physics.Raycast(pos.transform.position, Vector3.down, out hit, 5f, casillaLayerMask);

        if (raycastHit)
        {
            GameObject casilla = hit.collider.gameObject;
            nameCasilla = casilla.name;
        }
        //Debug.Log(nameCasilla);
        return nameCasilla;
    }

    public void setIfMoving(bool mov)
    {
        moving = mov;
    }

    public bool getIfMoving()
    {
        return moving;
    }

    public void setMovCounter(int movement)
    {
        movCounter = movement;
    }

    public int getterMovCounter()
    {
        return movCounter;
    }

    public void movCounterUp()
    {
        movCounter++;
    }

    public void setIsHiding(bool hid)
    {
        hiding = hid;
    }

    public bool getIfHiding()
    {
        return hiding;
    }
}