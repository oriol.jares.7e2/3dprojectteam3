using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerThrow : MonoBehaviour
{
    public LayerMask casillaLayerMask;
    public Material casillaInRangeMaterial;
    public Material casillaDefaultMaterial;
    public Material casillaHidingMaterial;
    public Material casillaWinMaterial;
    private bool throwing;
    private bool moving;

    public TextMeshProUGUI paperBallCounterUI;

    public int throwCounter;

    private void Start()
    {
        throwCounter = 2;
    }

    private void Update()
    {
        paperBallCounterUI.text = throwCounter.ToString();
        moving = this.gameObject.GetComponent<PlayerMovement>().getIfMoving();
        distract();
    }

    private void distract()
    {
        Collider[] casillas = Physics.OverlapSphere(this.gameObject.transform.position, 5.5f, casillaLayerMask);
        if (Input.GetKeyDown(KeyCode.Space) && !moving && throwCounter > 0 && !getIfThrowing())
        {
            throwCounter--;
            throwing = true;
            casillas = Physics.OverlapSphere(this.gameObject.transform.position, 5.5f, casillaLayerMask);
            for (int i = 0; i < casillas.Length; i++)
            {
                casillas[i].gameObject.GetComponent<MeshRenderer>().material = casillaInRangeMaterial;
                casillas[i].gameObject.GetComponent<OnMouseClick>().setIsInRange(true);
            }
        }
        if ((Input.GetMouseButton(0) || Input.GetMouseButton(1)) && getIfThrowing())
        {
            //Click izquierdo lanza
            throwing = false;
            for (int i = 0; i < casillas.Length; i++)
            {
                casillas[i].gameObject.GetComponent<OnMouseClick>().setIsInRange(false);
                if (casillas[i].gameObject.CompareTag("Hiding"))
                {
                    casillas[i].gameObject.GetComponent<MeshRenderer>().material = casillaHidingMaterial;
                } else if (casillas[i].gameObject.CompareTag("Tutorial") || casillas[i].gameObject.CompareTag("Scorerelevancy"))
				{
					casillas[i].gameObject.GetComponent<MeshRenderer>().material = casillaWinMaterial;
				}
                else
                {
                    casillas[i].gameObject.GetComponent<MeshRenderer>().material = casillaDefaultMaterial;
                }
            }
            //Click derecho cancela lanzamiento
            if (Input.GetMouseButton(1))
            {
                throwCancel();
            }
        }
    }

    public bool getIfThrowing()
    {
        return throwing;
    }

    public void throwCancel()
    {
        throwCounter++; 
    }

}
