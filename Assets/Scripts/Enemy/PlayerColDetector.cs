using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class PlayerColDetector : MonoBehaviour
{
    private NotesCount count;

    void Awake()
    {
        count = GameObject.Find("NotesCount").GetComponent<NotesCount>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            //Play death audio clip
            FindObjectOfType<AudioManager>().Play("Death");

            count.resetNotesCount();
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }
    }
}
