using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{

    public LayerMask caminoLayerMask;
    public LayerMask casillaLayerMask;

    //Los gameobjects de detección
    public GameObject rCO;
    public GameObject rCC;
    public GameObject rCGr;
    public GameObject rCP;
    public GameObject rCRight;
    public GameObject rCTop;
    public GameObject rCLeft;
    public GameObject rCBottom;
    public GameObject rCTopLeft;
    public GameObject rCTopRight;
    public GameObject rCBottomRight;
    public GameObject rCBottomLeft;

    private bool moving = false;
    private bool playerHiding = false;

    private float timesMoved;
    private float distanceMoved = 0f;
    private GameObject playerChar;
    private Vector3 playerVector3;
    private Vector3 vector3Final;
    private Vector3 direction;
    private Vector3 casillaPos;

    private bool isDistracted = false;

    public GameObject player;

    /*
     * Se llama una vez hecho clic en una casilla cuando el jugador se puede mover, guarda en un array todas las
     * cassilas a las que se puede ir y entonces se escoge una aleatoriamente (por ahora) y se envia a move()
     */
    public void moveEnemy()
    {
        RaycastHit hit;

        List<GameObject> posiblesCasillas = new List<GameObject>();
        List<GameObject> posiblesMovimientos = new List<GameObject>();

        if (Physics.Raycast(rCO.transform.position, Vector3.down, out hit, 5f, casillaLayerMask))
        {
            bool raycastHit = Physics.Raycast(rCO.transform.position, Vector3.left, 2f, caminoLayerMask);
            if(raycastHit) {
                posiblesCasillas.Add(rCO);
                posiblesMovimientos.Add(rCO);
            }
            
        }
        if (Physics.Raycast(rCC.transform.position, Vector3.down, out hit, 5f, casillaLayerMask))
        {
            bool raycastHit = Physics.Raycast(rCC.transform.position, Vector3.back, 2f, caminoLayerMask);
            if(raycastHit) {
                posiblesCasillas.Add(rCC);
                posiblesMovimientos.Add(rCC);

            }
        }
        if (Physics.Raycast(rCGr.transform.position, Vector3.down, out hit, 5f, casillaLayerMask))
        {
            bool raycastHit = Physics.Raycast(rCGr.transform.position, Vector3.right, 2f, caminoLayerMask);
            if(raycastHit) {
                posiblesCasillas.Add(rCGr);
                posiblesMovimientos.Add(rCGr);
            }
        }
        if (Physics.Raycast(rCP.transform.position, Vector3.down, out hit, 5f, casillaLayerMask))
        {
            bool raycastHit = Physics.Raycast(rCP.transform.position, Vector3.forward, 2f, caminoLayerMask);
            if(raycastHit) {
                posiblesCasillas.Add(rCP);
                posiblesMovimientos.Add(rCP);
            }
        }
        if (Physics.Raycast(rCRight.transform.position, Vector3.down, out hit, 5f, casillaLayerMask))
        {
            bool raycastHit = Physics.Raycast(rCRight.transform.position, Vector3.left, 2f, caminoLayerMask);
            if (raycastHit)
            {
                posiblesCasillas.Add(rCRight);
            }

        }
        if (Physics.Raycast(rCTop.transform.position, Vector3.down, out hit, 5f, casillaLayerMask))
        {
            bool raycastHit = Physics.Raycast(rCTop.transform.position, Vector3.back, 2f, caminoLayerMask);
            if (raycastHit)
            {
                posiblesCasillas.Add(rCTop);
            }
        }
        if (Physics.Raycast(rCLeft.transform.position, Vector3.down, out hit, 5f, casillaLayerMask))
        {
            bool raycastHit = Physics.Raycast(rCLeft.transform.position, Vector3.right, 2f, caminoLayerMask);
            if (raycastHit)
            {
                posiblesCasillas.Add(rCLeft);
            }
        }
        if (Physics.Raycast(rCBottom.transform.position, Vector3.down, out hit, 5f, casillaLayerMask))
        {
            bool raycastHit = Physics.Raycast(rCBottom.transform.position, Vector3.forward, 2f, caminoLayerMask);
            if (raycastHit)
            {
                posiblesCasillas.Add(rCBottom);
            }
        }
        if (Physics.Raycast(rCTopLeft.transform.position, Vector3.down, out hit, 5f, casillaLayerMask))
        {
            bool raycastHit = Physics.Raycast(rCTopLeft.transform.position, Vector3.right, 2f, caminoLayerMask);
            bool raycastHit2 = Physics.Raycast(rCTopLeft.transform.position, Vector3.back, 2f, caminoLayerMask);
            if (raycastHit || raycastHit2)
            {
                posiblesCasillas.Add(rCTopLeft);
            }

        }
        if (Physics.Raycast(rCTopRight.transform.position, Vector3.down, out hit, 5f, casillaLayerMask))
        {
            bool raycastHit = Physics.Raycast(rCTopRight.transform.position, Vector3.left, 2f, caminoLayerMask);
            bool raycastHit2 = Physics.Raycast(rCTopRight.transform.position, Vector3.back, 2f, caminoLayerMask);
            if (raycastHit || raycastHit2)
            {
                posiblesCasillas.Add(rCTopRight);
            }
        }
        if (Physics.Raycast(rCBottomRight.transform.position, Vector3.down, out hit, 5f, casillaLayerMask))
        {
            bool raycastHit = Physics.Raycast(rCBottomRight.transform.position, Vector3.left, 2f, caminoLayerMask);
            bool raycastHit2 = Physics.Raycast(rCBottomRight.transform.position, Vector3.forward, 2f, caminoLayerMask);
            if (raycastHit || raycastHit2)
            {
                posiblesCasillas.Add(rCBottomRight);
            }
        }
        if (Physics.Raycast(rCBottomLeft.transform.position, Vector3.down, out hit, 5f, casillaLayerMask))
        {
            bool raycastHit = Physics.Raycast(rCBottomLeft.transform.position, Vector3.right, 2f, caminoLayerMask);
            bool raycastHit2 = Physics.Raycast(rCBottomLeft.transform.position, Vector3.forward, 2f, caminoLayerMask);
            if (raycastHit || raycastHit2)
            {
                posiblesCasillas.Add(rCBottomLeft);
            }
        }

        GameObject playerPositionObject = checkPlayer();
        playerHiding = GameObject.Find("Player").GetComponent<PlayerMovement>().getIfHiding();

        if (!playerHiding)
        {
            //Si se detecta el jugador se pasa por el if, en caso contrario por el else
            if (playerPositionObject != null)
            {
                if (posiblesMovimientos.Count > 0)
                {
                    if (playerPositionObject != rCO && playerPositionObject != rCP && playerPositionObject != rCC && playerPositionObject != rCGr)
                    {
                        //TODO comprobar camino
                        GameObject siguienteCasilla = checkOptimalPath(posiblesCasillas, posiblesMovimientos);
                        moveToPlayer(siguienteCasilla);
                    }
                    else
                    {
						//Play playerdetected audio clip
						FindObjectOfType<AudioManager>().Play("PlayerDetected");
						if (posiblesMovimientos.Contains(playerPositionObject))
                        {
                            moveToPlayer(playerPositionObject);
                        }
                        else
                        {
                            moveToPlayer(posiblesMovimientos[Random.Range(0, posiblesMovimientos.Count)]);
                        }
                    }
                }
            }
            else
            {
                if (posiblesMovimientos.Count > 0)
                {
                    move(posiblesMovimientos[Random.Range(0, posiblesMovimientos.Count)]);
                }
            }
        }
        else
        {
            if (posiblesMovimientos.Count > 0)
            {
                move(posiblesMovimientos[Random.Range(0, posiblesMovimientos.Count)]);
            }
        }
    }

    //A partir del tiempo que se quiera tardar, la distancia entre las casillas y los fps se calcula la distancia a desplazarse en cada frame
    private void move(GameObject gmObj) {
        playerChar = this.gameObject;
        if (!moving)
        {
            casillaPos = new Vector3(gmObj.transform.position.x, this.gameObject.transform.position.y, gmObj.transform.position.z);
            timesMoved = (2.5f * 0.02f) / 0.5f;
            distanceMoved = 0f;
            moving = true;
            playerVector3 = playerChar.transform.position;
            oldVector3 = playerVector3;
            vector3Final = casillaPos - playerVector3;
            direction = vector3Final.normalized;
        }
    }

    //En esta funcion se calcula la direccion optima para desplazarse hacia el jugador
    private void moveToPlayer(GameObject gmObj)
    {
        playerChar = this.gameObject;
        if (!moving)
        {
            casillaPos = new Vector3(gmObj.transform.position.x, this.gameObject.transform.position.y, gmObj.transform.position.z);
            timesMoved = (2.5f * 0.02f) / 0.5f;
            distanceMoved = 0f;
            moving = true;
            playerVector3 = playerChar.transform.position;
            oldVector3 = playerVector3;
            vector3Final = casillaPos - playerVector3;
            direction = vector3Final.normalized;
        }
    }

    //En esta funcion se calcula la direccion optima para desplazarse hacia la casilla seleccionada por el jugador
    public void moveToCasilla(GameObject gmObj)
    {
        playerChar = this.gameObject;
        if (!moving)
        {
            casillaPos = new Vector3(gmObj.transform.position.x, this.gameObject.transform.position.y, gmObj.transform.position.z);
            timesMoved = (2.5f * 0.02f) / 0.5f;
            distanceMoved = 0f;
            moving = true;
            playerVector3 = playerChar.transform.position;
            oldVector3 = playerVector3;
            vector3Final = casillaPos - playerVector3;
            direction = vector3Final.normalized;
        }
    }

    /*
     * Esta funcion se usa para comprobar si el jugador esta en alguna casilla dentro del radio de deteccion del enemigo.
     * Si lo detecta devuelve el GameObject que lo ha detectado, en caso contrario devuelve null.
     */
    private GameObject checkPlayer()
    {
        if (rCC.transform.position.x == player.transform.position.x && rCC.transform.position.z == player.transform.position.z)
        {
            return rCC;
        }
        if (rCO.transform.position.x == player.transform.position.x && rCO.transform.position.z == player.transform.position.z)
        {
            return rCO;
        }
        if (rCP.transform.position.x == player.transform.position.x && rCP.transform.position.z == player.transform.position.z)
        {
            return rCP;
        }
        if (rCGr.transform.position.x == player.transform.position.x && rCGr.transform.position.z == player.transform.position.z)
        {
            return rCGr;
        }
        if (rCTop.transform.position.x == player.transform.position.x && rCTop.transform.position.z == player.transform.position.z)
        {
            return rCTop;
        }
        if (rCRight.transform.position.x == player.transform.position.x && rCRight.transform.position.z == player.transform.position.z)
        {
            return rCRight;
        }
        if (rCBottom.transform.position.x == player.transform.position.x && rCBottom.transform.position.z == player.transform.position.z)
        {
            return rCBottom;
        }
        if (rCLeft.transform.position.x == player.transform.position.x && rCLeft.transform.position.z == player.transform.position.z)
        {
            return rCLeft;
        }
        if (rCTopLeft.transform.position.x == player.transform.position.x && rCTopLeft.transform.position.z == player.transform.position.z)
        {
            return rCTopLeft;
        }
        if (rCTopRight.transform.position.x == player.transform.position.x && rCTopRight.transform.position.z == player.transform.position.z)
        {
            return rCTopRight;
        }
        if (rCBottomRight.transform.position.x == player.transform.position.x && rCBottomRight.transform.position.z == player.transform.position.z)
        {
            return rCBottomRight;
        }
        if (rCBottomLeft.transform.position.x == player.transform.position.x && rCBottomLeft.transform.position.z == player.transform.position.z)
        {
            return rCBottomLeft;
        }
        return null;
    }

    private GameObject checkOptimalPath(List<GameObject> posiblesCasillas, List<GameObject> posiblesMovimientos)
    {
        Vector3 enemyPosition = this.gameObject.transform.position;
        Vector3 playerPosition = player.transform.position;
        float distanceX = (enemyPosition.x - playerPosition.x) * -1;
        float distanceZ = (enemyPosition.z - playerPosition.z) * -1;
        Debug.Log(distanceX);
        Debug.Log(distanceZ);
        for(int i = 0; i < posiblesCasillas.Count; i++)
        {
            Debug.Log(posiblesCasillas[i]);
        }
        if(Mathf.Abs(distanceX) == Mathf.Abs(distanceZ) && distanceX != 0f)
        {
            if(distanceX == 2.5f && distanceZ == 2.5f)
            {
                Debug.Log("here");
                if(posiblesCasillas.Contains(rCC) && posiblesCasillas.Contains(rCTopRight) && Physics.Raycast(rCTopRight.transform.position, Vector3.left, 2f, caminoLayerMask))
                {
                    return rCC;
                }
                else if(posiblesCasillas.Contains(rCO) && posiblesCasillas.Contains(rCTopRight) && Physics.Raycast(rCTopRight.transform.position, Vector3.back, 2f, caminoLayerMask))
                {
                    return rCO;
                }
                else
                {
                    return posiblesMovimientos[Random.Range(0, posiblesMovimientos.Count)];
                }
            }
            else if(distanceX == -2.5f && distanceZ == 2.5f)
            {
                Debug.Log("over here");
                if (posiblesCasillas.Contains(rCC) && posiblesCasillas.Contains(rCTopLeft) && Physics.Raycast(rCTopLeft.transform.position, Vector3.right, 2f, caminoLayerMask))
                {
                    return rCC;
                }
                else if (posiblesCasillas.Contains(rCGr) && posiblesCasillas.Contains(rCTopLeft) && Physics.Raycast(rCTopLeft.transform.position, Vector3.back, 2f, caminoLayerMask))
                {
                    return rCGr;
                }
                else
                {
                    return posiblesMovimientos[Random.Range(0, posiblesMovimientos.Count)];
                }
            }
            else if(distanceX == -2.5f && distanceZ == -2.5f)
            {
                Debug.Log("right here");
                if (posiblesCasillas.Contains(rCP) && posiblesCasillas.Contains(rCBottomLeft) && Physics.Raycast(rCBottomLeft.transform.position, Vector3.right, 2f, caminoLayerMask))
                {
                    return rCP;
                }
                else if (posiblesCasillas.Contains(rCGr) && posiblesCasillas.Contains(rCBottomLeft) && Physics.Raycast(rCBottomLeft.transform.position, Vector3.forward, 2f, caminoLayerMask))
                {
                    return rCGr;
                }
                else
                {
                    return posiblesMovimientos[Random.Range(0, posiblesMovimientos.Count)];
                }
            }
            else if(distanceX == 2.5f && distanceZ == -2.5f)
            {
                Debug.Log("not there");
                if (posiblesCasillas.Contains(rCP) && posiblesCasillas.Contains(rCBottomRight) && Physics.Raycast(rCBottomRight.transform.position, Vector3.left, 2f, caminoLayerMask))
                {
                    return rCP;
                }
                else if (posiblesCasillas.Contains(rCO) && posiblesCasillas.Contains(rCBottomRight) && Physics.Raycast(rCBottomRight.transform.position, Vector3.forward, 2f, caminoLayerMask))
                {
                    return rCO;
                }
                else
                {
                    return posiblesMovimientos[Random.Range(0, posiblesMovimientos.Count)];
                }
            }
        }
        else if(distanceZ == 5f)
        {
            if (posiblesCasillas.Contains(rCC) && posiblesCasillas.Contains(rCTop))
            {
                return rCC;
            }
        }
        else if (distanceZ == -5f)
        {
            if (posiblesCasillas.Contains(rCP) && posiblesCasillas.Contains(rCBottom))
            {
                return rCP;
            }
        }
        else if (distanceX == 5f)
        {
            if (posiblesCasillas.Contains(rCO) && posiblesCasillas.Contains(rCRight))
            {
                return rCO;
            }
        }
        else if (distanceX == -5f)
        {
            if (posiblesCasillas.Contains(rCGr) && posiblesCasillas.Contains(rCLeft))
            {
                return rCGr;
            }
        }
        if (distanceX == 0f && distanceZ == 0f)
        {
            Debug.Log("Enemy and player in same area");
        }
        return posiblesMovimientos[Random.Range(0, posiblesMovimientos.Count)];
    }

    Vector3 oldVector3;

    //Comprueba si se puede mover y lo hace
    void FixedUpdate()
    {
        if (moving && playerVector3.x != casillaPos.x || playerVector3.y != casillaPos.y || playerVector3.z != casillaPos.z)
        {
            float newXValue = (float)System.Math.Round(playerVector3.x + (timesMoved * direction.x), 2);
            float newYValue = (float)System.Math.Round(playerVector3.y + (timesMoved * direction.y), 2);
            float newZValue = (float)System.Math.Round(playerVector3.z + (timesMoved * direction.z), 2);
            playerVector3 = new Vector3(newXValue, newYValue, newZValue);
            playerChar.transform.position = playerVector3;
            if (playerVector3 == casillaPos)
            {
                moving = false;
            }
        }
    }
}
